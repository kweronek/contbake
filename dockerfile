#
# Dockerfile to create lightweight go-executables by means of staged dockercontainer
#
#
# Image from github to support golang build on alpine linux
FROM golang:alpine as builder
#
# Create a directory for build
RUN mkdir /build
#
# copies all files from the source-directory to to the build directory
ADD . /build/
#
# set the actual WORKDIR to the build-directory
WORKDIR /build
#
# compiles and links the go executable as main an copies it to the actual directory .
# RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags
# "-static"' https://gitlab.informatik.fb2.hs-intern.de/weronek/contbake"' -o main .
#
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main
#
# generates a new empty container without overhead for Linux and Go
FROM scratch
#
# copies the executable from /build/main to /app/
COPY --from=builder /build/main /app/
#
# sets the WORKDIR to /app
WORKDIR /app
#
# starts the executable main in the actual WORKDIR (/app)
CMD ["./main"]
#
###### EOF ######
