package main

// version test0.1

import (
	"fmt"
	"os"
	"time"
)

// Main Program
func main() {

	// setup
	tStart := time.Now().Format(time.StampMilli)
	fmt.Println("Container-Bake gestartet!")

	// loop
	for i := 0; i < 10; i++ {
		fmt.Printf("Tag: | PID: %d | stamp: %v | start: %v\n", os.Getpid(), time.Now().Format(time.StampMilli), tStart)
		time.Sleep(10 * time.Second)
	}

	fmt.Println("Container-Bake beendet!")
}
